const yaml = require('js-yaml'); //initialize js-yaml
const fs   = require('fs'); //initialize filestream
const axios = require('axios');
const express = require('express')
const formidable = require('formidable');
const app = express()
const puerto = 8023
const puerto_cb = 8024
app.set('views', './views')
app.set('view engine', 'pug')
const path = require('path');
app.use("/static", express.static(path.join(__dirname, "public")));
const urn = 'urn:ngsi-ld'
const funciones = require('./funciones.js');
const mongoId = require('mongodb').ObjectID;

//Mongoose to swagger
const mongoose = require('mongoose');
const m2s = require('mongoose-to-swagger');

// --------------------------- Llama a 82.223.81.195:6601 -------------------------------
const orion = axios.create({
    baseURL: 'http://82.223.81.195:6601'
});

// --------------------------- Llama a 82.223.81.195:6602 -------------------------------
const contextProvider = axios.create({
    baseURL: 'http://82.223.81.195:6602'
});

// --------------------------- Llama a localhost:8000 Fakeson -------------------------------
const fakesonServer = axios.create({
    baseURL: 'http://localhost:8000'
});

// --------------------------- Llama a http://repositorio-tebaida.catedraturismosostenible.es/api/items -------------------------------
const omekaServer = axios.create({
    baseURL: 'http://repositorio-tebaida.catedraturismosostenible.es'
});

//Lectura desde la Entidad Padre a la Entidad Hijo
///v2/entities/?q=refStore==urn:ngsi-ld:Store:${id}&options=count&attrs=type&attrs=name`)

// --------------------------- Middelware -------------------------------------------------
//esto es para parsear el body
app.use(express.urlencoded({ extended: true}))
app.use(express.json({limit:'5Mb'}))

// --------------------------- Servidor Local ---------------------------------------------

// Esto para hacer async await
// app.get('/', async (req, res) => {
//     const query = await axios.get('http://82.223.81.195:6601');
//     res.render('index', { version: query.data });
// });

// GET version
app.get('/version', (req, res) => {   
    orion.get('/version')
        .then(function (response) {
            let version = response.data; 
            res.status(response.status).render('version', { title: 'PUG test', version: version});
        })
        .catch(function (error) {
            console.log(error);
            res.status(error.response.status).render('index', { title: 'Error VERSION', message: error.response.data.error});
    })
})

// GET entidades
app.get('/v2/entities', (req, res) => {
    orion.get('/v2/entities')
        .then(function (response) {
            let entities = response.data; 
            res.status(response.status).send(entities)
        })
        .catch(function (error) {
            console.log(error);
            res.status(error.response.status).render('index', { title: 'Error GET', message: error.response.data.error});
    })   
})

//GET urn:ngsi-ld:<tipo-de-entidad>:<id-de-la-identidad>
app.get(`/v2/entities/get::tipo::id`, (req, res) => { //OJO a los dobles dos puntos
    let tipo = req.params.tipo
    let id = req.params.id
    let options = req.query.options
    orion.get(`/v2/entities/${urn}:${tipo}:${id}`, {
            params: {
                options: options
            }
        })
        .then(function (response) {
            let id_entities = response.data;
            res.status(response.status).send(id_entities)
        })
        .catch(function (error) {
            console.log(error.response.data.error + ` ${tipo}:${id}`)
            res.status(error.response.status).render('index', { title: 'Error GET', message: error.response.data.error + ` ${tipo}:${id}`});
    })  
})

// GET por entidad-identidad-?tipo
app.get(`/v2/entities/get::tipo::id`, (req, res) => { //OJO a los dobles dos puntos
    let tipo = req.params.tipo
    let id = req.params.id
    let type = req.query.type
    orion.get(`/v2/entities/${urn}:${tipo}:${id}`, {
            params: {
                type: type
            }
        })
        .then(function (response) {
            let tipo_entities = response.data;
            res.status(response.status).send(tipo_entities)
        })
        .catch(function (error) {
            console.log(error.response.data.error + ` ${tipo}:${id}`)
            res.status(error.response.status).render('index', { title: 'Error GET', message: error.response.data.error + ` ${tipo}:${id}`});
    })  
})

// POST
app.post('/v2/entities', (req, res) => {  
    let producto = req.body
    orion.post('/v2/entities', producto)
        .then(function (response) {
            res.status(response.status).send(response.data)
        })
        .catch(function (error) {
            console.log(error);
            res.status(error.response.status).render('index', { title: 'Error POST', message: error.response.data.error});
    }) 
})

// PUT actualizar un atributo
app.put(`/v2/entities/put::tipo::id/attrs/:atributo/value`, (req, res) => { //OJO a los dobles dos puntos
    let tipo = req.params.tipo
    let id = req.params.id
    let atributo = req.params.atributo
    let value = req.body.value
    orion.put(`/v2/entities/${urn}:${tipo}:${id}/attrs/${atributo}/value`, value, {
            headers: { //desde el postman se pasa en formato json pero fiware quiere text/plain
                'Content-Type': 'text/plain',
            }
        })
        .then(function (response) {
            let tipo_entities = response.data;
            res.status(response.status).send(tipo_entities)
        })
        .catch(function (error) {
            console.log(error.response.data.error + ` ${tipo}:${id}`);
            res.status(error.response.status).render('index', { title: 'Error PUT', message: error.response.data.error + ` ${tipo}:${id}`});
    })  
})

// DELETE
app.delete(`/v2/entities/delete::tipo::id`, (req, res) => { 
    let tipo = req.params.tipo
    let id = req.params.id
    orion.delete(`/v2/entities/${urn}:${tipo}:${id}`)
        .then(function (response) {
            let del_response = response.data
            res.status(response.status).send(del_response)
        })
        .catch(function (error) {
            console.log(error.response.data.error + ` ${tipo}:${id}`);
            res.status(error.response.status).render('index', { title: 'Error DELETE', message: error.response.data.error + ` ${tipo}:${id}`});
    }) 
})

// app.delete('/v2/entities', (req, res) => {  
//     let producto = req.body
//     orion.delete('/v2/entities', producto)
//         .then(function (response) {
//             res.status(response.status).send(response.data)
//         })
//         .catch(function (error) {
//             console.log(error);
//             res.status(error.response.status).render('index', { title: 'Error DELETE', message: error.response.data.error});
//     }) 
// })

//Context broker
app.get('/health/static', (req, res) => {   
    contextProvider.get('/health/static')
        .then(function (response) {
            let hStatic = response.data; 
            //res.status(200).render('version', { title: 'PUG test', version: version});
            res.status(response.status).send(hStatic)
        })
        .catch(function (error) {
            console.log(error);
            res.status(error.response.status).render('index', { title: 'Error VERSION', message: error.response.data.error});
    })
})

app.post('/random/weatherConditions/op/query', (req, res) => {  //lee varios valores
    let weatherConditions = req.body
    contextProvider.post('/random/weatherConditions/op/query', weatherConditions)
        .then(function (response) {
            res.status(response.status).send(response.data)
        })
        .catch(function (error) {
            //console.log(error);
            res.status(error.response.status).render('index', { title: 'Error POST', message: error.response.data.error});
    }) 
})

app.get('/v2/registrations', (req, res) => {   // lee los Context Providers registrados
    contextProvider.get('/v2/registrations')
        .then(function (response) {
            let reg = response.data; 
            //res.status(200).render('version', { title: 'PUG test', version: version});
            res.status(response.status).send(reg)
        })
        .catch(function (error) {
            //console.log(error);
            res.status(error.response.status).render('index', { title: 'Error reg', message: 'Not found'});
    })
})

// Grafana
app.get('/grafana', (req, res) => {   
    let fecha1 = funciones.timestamp_to_date(1618041658000); //string
    let fecha2 = funciones.timestamp_to_date(1618214231531); //string
    res.render('grafana', { title: 'Grafana Fiware', fechas: [fecha1, fecha2]});
})

//ejemplo yaml
// try {
//   const result = yaml.load(fs.readFileSync('public/example.yaml', 'utf8'));
//   console.log(result);
// } catch (e) {
//   console.log(e); //catch exception
// }

// ejemplo fakeson
app.get('/fakeson/:fich', (req, res) => {
    let ruta_fich = req.params.fich;
    fakesonServer.get(`/${ruta_fich}`)
        .then(function (response) {
            let fakesonjson = response.data; 
            res.status(200).json(fakesonjson)
            fs.writeFileSync(`${ruta_fich}.json`, JSON.stringify(fakesonjson, null, 2),'utf8')
            let COMANDO_CONVERTER = [];
            if(!ruta_fich.includes("lista")){
                //let num_elementos = Object.keys(fakesonjson).length; //cantidad de valores en cada sensor
                COMANDO_CONVERTER = `node convert_server.js ${ruta_fich}.json ${ruta_fich}_mongo.json 0`;
            }else{
                //fs.writeFileSync(`${ruta_fich}_mongo.json`, JSON.stringify(fakesonjson, null, 2),'utf8')
                COMANDO_CONVERTER = `node convert_server.js ${ruta_fich}.json ${ruta_fich}_mongo.json 1`;
                //console.log(`Fichero ${ruta_fich}_mongo.json procesado ok para importar en MongoDB`);
            }
            funciones.runConverters(COMANDO_CONVERTER)
                .then(res => console.log(res))
                .catch(err => console.log(err))
        })
        .catch(function (error) {
            res.status(error.response.status).render('index', { title: 'Error fakeson', message: error.response.data});
        })
})

//ejemplo mongoose to swagger
const Cat = mongoose.model('Cat', { 
    name: {
        type: String,
        /**
         * Custom Properties
         * `description` is enabled by default
         */
        description: 'Name of the cat', // description is enabled by default
        bar: 'baz' // custom prop
    },
    color: String
});

const options = { 
    /**
     * Whitelist of custom meta fields.
     */
    props: ['bar'],
    /**
     * Fields to omit from model root. "__v" is omitted by default
     */
    omitFields: ['_id', 'color'], 
};

const swaggerSchema = m2s(Cat, options);
console.log(swaggerSchema);

// diccionario pruebas
const units = [{
    medida: 'kg/m²',
    cefact: '28'
}, {
   medida: 'dB',
   cefact: '2N'
}];
const dUnits = funciones.convertArrayToObject(units);
console.log(dUnits['dB']);
// https://stackoverflow.com/questions/11393200/how-to-find-value-using-key-in-javascript-dictionary/11393226

// if  (dUsers) {// Para guardan un nuevo usuario seria
//     dUsers[newUser.id] = newUser;
// }



//////////////////////////////////////////////////// OMEKA
app.get('/api/omeka/items/:id', (req, res) => {   
    omekaServer.get(`/api/items/${req.params.id}`)
        .then(function (response) {
            let hStatic = response.data; 
            //res.status(200).render('version', { title: 'PUG test', version: version});
            res.status(response.status).send(hStatic)
        })
        .catch(function (error) {
            console.log(error);
            res.status(error.response.status).render('index', { title: 'Error VERSION', message: error.response.data.error});
    })
})

app.get('/api/omeka/items', (req, res) => {   
    omekaServer.get('/api/items')
        .then(function (response) {
            let hStatic = response.data; 
            //res.status(200).render('version', { title: 'PUG test', version: version});
            res.status(response.status).send(hStatic)
        })
        .catch(function (error) {
            console.log(error);
            res.status(error.response.status).render('index', { title: 'Error VERSION', message: error.response.data.error});
    })
})

///////////////////////// FORMIDABLE
let log = require('single-line-log').stdout;
app.get('/', (req, res) => {
    res.send(`
      <h2>With <code>"express"</code> npm package</h2>
      <form action="/api/upload" enctype="multipart/form-data" method="post">
        <div>Text field title: <input type="text" name="title" /></div>
        <div>File: <input type="file" name="someExpressFiles" multiple="multiple" /></div>
        <input type="submit" value="Upload" />
      </form>
    `);
  });
  
  app.post('/api/upload', (req, res, next) => {
      const options = {
          multiples: true,
          uploadDir: `${__dirname}/files/`,
          keepExtensions: true,
          allowEmptyFiles: false,
          //minFileSize: 10
          //maxFileSize:10
      };
    const form = formidable(options);
 
    form.parse(req, (err, fields, files) => {
      if (err) {
        next(err);
        return;
      }
      res.json(datos = { 
        fields, 
        files 
      });
    });

    form.on('fileBegin', function(name, file){
        archivo = new mongoId();
        tipo = file.type.split('/')[0]; //mime
        formato = file.type.split('/')[1]; //mime
        if (formato === 'jpeg') formato =  formato.replace('e','');
        file.name = archivo + '.' + formato;
        file.path = options.uploadDir + archivo + '.' + formato; //file.name;
        // debug('nombre: ' + file.name);
        // debug('type: ' + file.type);
        // debug('tipo: ' + tipo);
        // debug('formato: ' + formato);
    })
    

        //     form.on('progress', function(bytesReceived, bytesExpected) {
        //     let percent = (bytesReceived / bytesExpected * 100) | 0;
        //     let salida = 'Uploading: %' + percent;
        //     log(salida);
        // })
    // form.on('end', () => {
    //     res.json({
    //         options,
    //         datos
    //     })
    // });
  });


// No existe ruta
app.use(function(req, res, next) {
    res.status(404).render('error_ruta', { title: 'Error ruta', message: 'La ruta solicitada no existe'});
});

app.listen(puerto, () => {
  console.log(`Servidor activo en  http://localhost:${puerto}`)
})
