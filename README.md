# Fiware_Getting_Started

Tutorial NGSI-V2

Ejecutar:
* node 2server.js

Rutas:
* /version GET
* /v2/entities GET POST
* /v2/entities/urn:ngsi-ld::tipo::id GET
* /v2/entities/urn:ngsi-ld::tipo::id?type GET
* /v2/entities/urn:ngsi-ld::tipo::id/attrs/:atributo/value PUT
* /v2/entities/urn:ngsi-ld::tipo::id DELETE