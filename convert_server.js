// codifica de ucs2 a utf8 y
// formatea correctamente el json para importar manualmente desde compass a mongoDB en caso de que sea un array de objetos
// USAR SOLO SI EL FICHERO .fakeson HA SIDO HECHO POR LLAMADA GET AL SERVIDOR DE LA gema_fakeson

const fs = require('fs');

let fich_a_convertir = process.argv[2];
let fich_convertido = process.argv[3];
let es_array = process.argv[4]; //si el fich es un array de objetos, 1; si no 0

if ((fich_a_convertir == null) || (fich_convertido == null)) {
    console.log('Faltan ficheros');
    return;
}
if (!fs.existsSync(fich_a_convertir)){
    console.log(`No existe el fichero a convertir ${fich_a_convertir}`);
    return;
}
if (es_array == null) {
    console.log('Falta el ultimo argumento');
    return;
  }

//Conversion a utf-8 para cuando se usa fakeson por linea de comandos
// let content = fs.readFileSync(fich_a_convertir, {encoding:'ucs2'});
// fs.writeFileSync(fich_convertido, content, {encoding:'utf8'});
// console.log('Fichero codificado a UTF-8')


//Hacemos copia del fich_a_convertir para que si no existe fich_convertido no de error
fs.copyFileSync(fich_a_convertir, fich_convertido, 0, (err) => { //para versiones > Node.js 8.5.0 
    if (err) throw err;
    //console.log('fichero copiado');
  });


/////////////////////Usage: node .\convert.js fich_a_convertir.json fich_convertido.json 1|0

//Para convertir de utf8 a utf16le https://gist.github.com/zoellner/4af04a5a8b51f04ad653e26d3b7181ec
//Contar lineas https://stackoverflow.com/questions/12453057/node-js-count-the-number-of-lines-in-a-file
//Eliminar lineas https://stackoverflow.com/questions/38843016/how-to-remove-one-line-from-a-txt-file
//Prepend https://stackoverflow.com/questions/15423774/nodejs-prepending-to-a-file

const removeLines = (data, lines = []) => {
    return data
        .split('\n')
        .filter((val, idx) => lines.indexOf(idx) === -1)
        .join('\n');
}

function countFileLines(fich_convertido){
    return new Promise((resolve, reject) => {
      let lineCount = 0;
      fs.createReadStream(fich_convertido)
          .on("data", (buffer) => {
              let idx = -1;
              lineCount--; // Because the loop will run once for idx=-1
              do {
                idx = buffer.indexOf(10, idx+1);
                lineCount++;
              } while (idx !== -1);
          }).on("end", () => {
              resolve(lineCount);
          }).on("error", reject);
    });
};

//Formateo fichero
if (es_array == 1){
  //contamos lineas del fichero
  countFileLines(fich_convertido)
      .then(result => {

          fs.readFile(fich_convertido, 'utf8', (err, data) => {
              if (err) throw err;
              //quitamos las lineas que no queremos con removeLines(data, [0, 1, result-1]), en este caso la 1, la 2 y la ultima;
              fs.writeFile(fich_convertido, removeLines(data, [0, 1, result]), 'utf8', function(err) {
                  if (err) throw err;
                  //añadimos [ como primera linea del fichero final;
                  const data = fs.readFileSync(fich_convertido)
                  const fd = fs.openSync(fich_convertido, 'w+')
                  const insert = new Buffer.from("[\r\n")
                  fs.writeSync(fd, insert, 0, insert.length, 0)
                  fs.writeSync(fd, data, 0, data.length, insert.length)
                  fs.close(fd, (err) => {
                      if (err) throw err;
                      console.log(`Fichero ${fich_convertido} procesado ok para importar en MongoDB`);
                  });
              });
          })

      })
}else if(es_array == 0){
    fs.readFile(fich_convertido, 'utf8', (err, data) => {
        if (err) throw err;
        //quitamos las lineas que no queremos con removeLines(data, [0]), en este caso la 1;
        fs.writeFile(fich_convertido, removeLines(data, [0]), 'utf8', function(err) {
            if (err) throw err;
            //añadimos [{ como primera linea del fichero final;
            const data = fs.readFileSync(fich_convertido)
            const fd = fs.openSync(fich_convertido, 'w+')
            const insert = new Buffer.from("[\r\n{\r\n")
            fs.writeSync(fd, insert, 0, insert.length, 0)
            fs.writeSync(fd, data, 0, data.length, insert.length)
            fs.close(fd, (err) => {
                if (err) throw err;
                //añadimos ] como ultima linea del fichero final;
                let stream = fs.createWriteStream(fich_convertido, {flags:'a'});
                stream.write("]");
                stream.end();
                console.log(`Fichero ${fich_convertido} procesado ok para importar en MongoDB`);
            });
        });
    });
}