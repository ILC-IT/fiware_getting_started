'use strict'
var {exec} = require("child_process");

module.exports = {
    timestamp_to_date: timestamp_to_date,
    runConverters:runConverters,
    convertArrayToObject:convertArrayToObject
}

function timestamp_to_date(fecha){
    let unix_timestamp = fecha
    // Create a new JavaScript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds.
    var date = new Date(unix_timestamp) // * 1000); //en grafana el timestamp ya viene en miliseconds
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();
    
    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    
    //console.log(formattedTime);
    //console.log(date.toLocaleString('es-ES', { timeZone: "CET" }))
    return date.toLocaleString('es-ES', { timeZone: "CET" });
}

function runConverters(comando){
    let promesa = (resolve,reject) =>{ 
        exec(comando, (error, stdout, stderr) => {
            //console.log(`${stdout}`); //esto es para que se vean los console.log dentro de convert_server.js
            //console.log(`${stderr}`);
            if (error !== null) {
                console.log(`exec error: ${error}`);
                reject(error);
            }else{
                resolve(`${stdout}`);
            }
        });
    }
    return new Promise(promesa)
}

function convertArrayToObject(array) {
    return array.reduce(function (obj, item) { 
        obj[item.medida] = item; 
        //console.log(obj)
        return obj; 
    }, {});
}